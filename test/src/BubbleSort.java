public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {45,22,21,40,70,4,7,11,50};
        for (int i = 0; i < array.length; i++) {
            for (int j = i+1; j < (array.length - 1); j++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
            System.out.println(array[i]);
        }
    }



}
