package Equals;

public class Main {
    public static void main(String[] args) {
        String first = "Dimas";
        first = first + " " + "Mulya";

        System.out.println(first);

        String second = "Dimas Mulya";
        System.out.println(second);

        System.out.println(first == second);
        System.out.println(first.equals(second));

        String third = "Dimas Mulya";

        System.out.println(second ==  third);
        System.out.println(second.equals(third));
    }
}
