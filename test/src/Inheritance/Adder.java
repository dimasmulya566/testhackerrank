package Inheritance;

public class Adder {
    Adder() {}

    int add(int a, int b) {
        return a + b;
    }
}
