package Inheritance.caraSederhana;

public class TestManager {
    public static void main(String[] args) {
        var manager = new Manager();
        manager.name = "Dimas";
        manager.sayHello("Budi");

        var supervisor = new Supervisor();
        supervisor.name = "Bambang";
        supervisor.sayHello("Budi");
    }
}
