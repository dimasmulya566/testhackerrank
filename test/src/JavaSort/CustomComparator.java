package JavaSort;

import java.util.Comparator;

public class CustomComparator implements Comparator<Student> {
    @Override
    public int compare(Student s1, Student s2) {
        if (s1.getCgpa() < s2.getCgpa()) return 1;
        if (s1.getCgpa() > s2.getCgpa()) return -1;

        if (s1.getFname() != s2.getFname()){
            return s1.getFname().compareTo(s2.getFname());
        }
        return s1.getId() - s2.getId();
    }
}
