package Polymorphism;

public class Senar extends Gitar{
    public String SpesifikJenis = "Memiliki 7 Senar yang terpasang";
    public String suaraGitar(){
        return "Gonjjrengg gonjrreeennggg";
    }

    public String suaraGitar(String customsuara){
        return super.suaraGitar()+" "+customsuara;
    }
}
