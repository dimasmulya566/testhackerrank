package Polymorphism;

public class TestGitar {
    public static void main(String[] args) {
        Gitar bahan = new Gitar();

        System.out.println(bahan.merek);
        System.out.println(bahan.tipe);

        Senar alat = new Senar();

        System.out.println(alat.SpesifikJenis);
        System.out.println(alat.suaraGitar());
    }

}
