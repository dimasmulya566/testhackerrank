package Polymorphism;

public class Test {
    public static void main(String[] args) {
        Employee employee = new Employee("DIMAS");
        employee.sayHello("BUDI");

        employee = new Manager("RANGGA");
        employee.sayHello("JAPIR");

        employee = new Supervisor("REGA");
        employee.sayHello("DIDI");

        sayHello(new Employee("RAGIL"));
        sayHello(new Manager("DODI "));
        sayHello(new Supervisor("DERI "));

    }

    static void sayHello(Employee employee){
        if(employee instanceof Manager){
            Manager manager = (Manager) employee;
            System.out.println("Hi " + manager.name + "Saya manager loh");
        } else if (employee instanceof Supervisor){
            Supervisor sp = (Supervisor) employee;
            System.out.println("Hi " + sp.name + " Saya adalah Supervisor");
        } else {
            System.out.println("Hi " + employee.name + " Saya adalah Employee Loh");
        }
    }
}
