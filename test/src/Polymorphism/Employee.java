package Polymorphism;

class Employee {
    String name;

    Employee(String name){
        this.name = name;
    }
    void sayHello(String name){
        System.out.println("hai " + name + ", nama employee saya adalah " + this.name);
    }
}

class Manager extends Employee{

    Manager(String name) {
        super(name);
    }
    void sayHello(String name){
        System.out.println("hai " + name + ", nama manager saya adalah " + this.name);
    }
}

class Supervisor extends Manager{

    Supervisor(String name) {
        super(name);
    }
    void sayHello(String name){
        System.out.println("hai " + name + ", nama manager saya adalah " + this.name);
    }
}