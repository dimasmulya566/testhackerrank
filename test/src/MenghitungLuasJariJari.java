import java.util.Scanner;

public class MenghitungLuasJariJari {
    public static void main(String[] args) {
        // membuat sebuah scan input untuk memasukan jumlah nilai yang di inginkan
        Scanner input = new Scanner(System.in);

        // Membuat sebuah nilai variable bertipe double dan integer(int)
        double luas, phi = 3.14;
        int r ;

        // Membuat sebuah kata untuk memasukan nilai inputan
        System.out.print("Masukan Jumlah Jari Jari = ");

        // membuat inputan bernilai integer
        r = input.nextInt();

        // menghitung luas lingkarang
        luas = phi*r;

        // mencetak hasil inputan
        System.out.println("Hasil Luas Lingkaran = " + luas);
    }
}
