public class SegitigaArray {

    public static void main(String[] args) {
        // membuat segita menggunakan bintang
        int x,y,z,a;

        for (x = 1; x <= 6; x++){
            for (y = 5; y >= x; y--){
                System.out.print(" ");
            }
            for (z = 1; z <= x; z++){
                System.out.print("*");
            }
            for (a =1; a <= x -1; a++){
                System.out.print("*");
            }
            System.out.println();
        }


        // membuat segita terbalik menggunakan angka
        int b,c,d;

        for (b = 5; b >= 1; b--){
            for (c = 5; c > b; c--){
                System.out.print(" ");
            }
            for (d = 1; d < (b * 2); d++){
                System.out.print(d);
            }

            System.out.println();
        }
    }
}
