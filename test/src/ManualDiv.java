public class ManualDiv {
    public static void main(String[] args) {
        System.out.println(solution(90, 11));
    }

    private static int solution(int x, int y){
        int result = 0;
        while (x >= y){
            x -= y;
            result++;
        }
        return result;
    }

}
