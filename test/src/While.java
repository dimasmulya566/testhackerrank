public class While {
    public static void main(String[] args) {
        int a = 1;

        System.out.println("contoh while \n");
        while (a <= 10 ){
            System.out.println("Cetak Angka ke " + a);
            a++;
        }

        int value = 1;
        System.out.println("======================================== \n");

        System.out.println("contoh doWhile \n");

        do {
            System.out.println("Cetak angka ke " + value);
            value++;
        } while (value <= 10);
    }
}
