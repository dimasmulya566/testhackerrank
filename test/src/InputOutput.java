import java.util.Scanner;

public class InputOutput {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Ini adalah input dan output");
        System.out.print("Masukan nilai Pertama = ");
        int value = input.nextInt();
        System.out.print("Masukan nilai Kedua = ");
        int anotherValue = input.nextInt();
        int result = value + anotherValue;
        System.out.println("Hasil Dari nilai pertama dan kedua = " + result);
    }
}
