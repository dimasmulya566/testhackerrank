import java.util.Scanner;

public class StringFormat {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputan = scanner.nextLine();
        titleSolution(inputan);
        normalSolution(inputan);

    }

    private static void titleSolution(String input){
        String result = " ";
        String[] arrayOfString = input.toLowerCase().split("\\s+");
        for (int i = 0; i < arrayOfString.length; i++) {
            String upperCaseFirstchar = arrayOfString[i].substring(0,1).toUpperCase();
            String lowerCaseNextChar = arrayOfString[i].substring(1).toLowerCase();
            result += upperCaseFirstchar + lowerCaseNextChar + " ";
        }
        System.out.println("upper case");
        System.out.println(result);
    }

    private static void normalSolution(String input){
        String result = "";
        result += input.substring(0,1).toUpperCase();
        result += input.substring(1).toLowerCase();
        System.out.println("lower case");
        System.out.println(result);
    }
}
