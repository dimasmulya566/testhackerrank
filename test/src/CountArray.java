import java.util.Arrays;

public class CountArray {
    public static void main(String[] args) {
        Integer[] array = {24,24,50,60,60,60,50,33,32};
        solution(array);
    }
    private static void solution(Integer[] array){
        Arrays.stream(array).distinct().forEach(integer -> {
            int counter = 0;
            for (int i = 0; i < array.length; i++) {
                if (integer.equals(array[i])){
                    counter++;
                }
            }
            System.out.println(integer + " Tampilkan " + counter + " kali");
        });
    }
}
