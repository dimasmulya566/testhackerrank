public class MultiThread extends Thread{
    public MultiThread() {
        super.run();

        for (int i = 0; i < 10; i++) {
            System.out.println("Cetak angka ke " + i);

            try {
                sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        MultiThread multiThread = new MultiThread();
        multiThread.run();
    }
}
