public class Angka {
    public static void main(String[] args) {
        int angka[] = {1,2,3,4,5,6,7,8,9,0};
        System.out.println("\t Ganjil & Genap pada Array");

        System.out.println("\n Genap");
        for (int i = 0; i < 10; i++) {
            if (angka[i] % 2 == 0){
                System.out.println(angka[i]);
            }
        }

        System.out.println("\n Ganjil : ");
        for (int j = 0; j<10 ; j++) {
            if(angka[j] %2 != 0){
                System.out.println(angka[j]);
            }
        }
    }
}
