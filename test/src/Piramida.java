public class Piramida{
    public static void main(String[] args) {
        int x, y;

        for (x = 7; x >= 0; x--) {
            System.out.println();
            for (y = 7; y > x; y--){
                System.out.print(y + " ");
            }
        }
    }
}
